# An implementation of Uncertainty-wise Test Modeling (UncerTum)#

This is an implementation of [UncerTum](https://link.springer.com/article/10.1007/s10270-017-0609-6) by [IBM Rational Software Architect](https://www.ibm.com/developerworks/downloads/r/architect/index.html).

### What is this repository for? ###

![Overview of UncerTum](image/overall.png)

To facilitate model-based testing (MBT) of Cyber-Physical Systems (CPSs) under uncertainty, we proposed Uncertainty Modeling Framework (UncerTum). UncerTum allows creating test ready models with uncertainty at three logical testing levels of CPSs: Application, Infrastructure, and Integration. The core of UncerTum is the UML Uncertainty Profile (UUP), which implements an existing uncertainty conceptual model, called U-Model. In addition, UncerTum defines a comprehensive set of UML Model Libraries extending the UML profile for Modeling and Analysis of Real-Time and Embedded Systems (MARTE), which can be used together with UUP. UncerTum also relies on UML Testing Profile (UTP) V.2 to construct test ready models. Finally, UncerTum defines concrete guidelines for supporting the use of UncerTum for creating and validating test ready models with uncertainty. 

* This implementation includes the profiles and libraries marked as *orange*. The guidelines marked as *blue* are presented in the [paper](https://link.springer.com/article/10.1007/s10270-017-0609-6)
* Version: V1
* For more information, please find the [paper](https://link.springer.com/article/10.1007/s10270-017-0609-6) and UncerTum [specification](https://www.simula.no/sites/default/files/publications/files/uupv1.pdf).

### How do I get set up? ###

To use this implementation for construction of the belief test ready model, please follow the steps: 

* Import this project in RSA
* Optional import [MARTE and libraries](http://www.omg.org/omgmarte/Tools.htm)
* Optional import [UTP and libraries](http://www.omg.org/spec/UTP/About-UTP/)
* Create Model project
* Add UUP core proflie (optional for MARTE and UTP) as _Applied Proflies_ in the "Details" option.
* Add Libraries (i.e. Measure Library) as _Model Libraries_ in the "Details" option.

### Who do I talk to? ###

* Man Zhang (manzhang@simula.no), Shaukat Ali (shaukat@simula.no), Tao Yue (tao@simula.no)
* Simula Research Laboratory (www.simula.no)
* U-Test project (http://www.u-test.eu/)